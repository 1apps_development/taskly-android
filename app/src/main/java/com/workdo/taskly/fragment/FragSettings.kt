package com.workdo.taskly.fragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.workdo.taskly.R
import com.workdo.taskly.activity.*
import com.workdo.taskly.activity.ActInvoiceSetting
import com.workdo.taskly.viewModel.AuthViewModel
import com.workdo.taskly.databinding.FragSettingsBinding
import com.workdo.taskly.networking.ApiClient
import com.workdo.taskly.remote.NetworkResponse
import com.workdo.taskly.util.ExtensionFunctions.loadUrlCircle
import com.workdo.taskly.util.SharePreference
import com.workdo.taskly.util.Utils
import kotlinx.coroutines.launch

class FragSettings : Fragment() {
    private val viewModel by viewModels<AuthViewModel>()

    private var _binding: FragSettingsBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragSettingsBinding.inflate(layoutInflater)

        initClickListeners()

        return binding.root


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


    }

    private fun initClickListeners() {
        binding.constraintSiteSettings.setOnClickListener {
            startActivity(Intent(requireActivity(), ActSiteSettings::class.java))
        }

        binding.constraintTaxes.setOnClickListener {
            startActivity(Intent(requireActivity(), ActTaxes::class.java))

        }

        binding.constraintPayment.setOnClickListener {
            startActivity(Intent(requireActivity(), ActPaymentDetails::class.java))

        }

        binding.constraintZoomMeeting.setOnClickListener {
            startActivity(Intent(requireActivity(), ActZoomDetails::class.java))
        }

        binding.constraintSlackSettings.setOnClickListener {
            startActivity(Intent(requireActivity(), ActSlackSettings::class.java))

        }

        binding.constraintTelegram.setOnClickListener {
            startActivity(Intent(requireActivity(), ActTelegramSettings::class.java))
        }

        binding.btnEditProfile.setOnClickListener {
            startActivity(Intent(requireActivity(), ActMyAccount::class.java))

        }

        binding.constraintTaskStages.setOnClickListener {
            startActivity(Intent(requireActivity(), ActTaskStages::class.java))

        }

        binding.constraintBugStages.setOnClickListener {
            startActivity(Intent(requireActivity(), ActTaskStages::class.java))

        }
        binding.constraintInvoices.setOnClickListener {
            startActivity(Intent(requireActivity(), ActInvoiceSetting::class.java))

        }
        binding.constraintLogOut.setOnClickListener {
            callLogoutApi()
        }

        val userProfile = SharePreference.getStringPref(requireActivity(), SharePreference.userProfile)
        binding.ivProfile.loadUrlCircle(userProfile)
        binding.tvUserName.text = SharePreference.getStringPref(requireActivity(), SharePreference.userName)
        binding.tvEmail.text = SharePreference.getStringPref(requireActivity(), SharePreference.userEmail)
    }


    private fun callLogoutApi() {
        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity()).logOut()) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val dataResponse = response.body.data

                    when (response.body.status) {
                        1 -> {
                            val intent = Intent(requireActivity(), ActLogin::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            startActivity(intent)
                            requireActivity().finish()
                        }

                        0 -> {
                            Utils.errorAlert(requireActivity(), response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), response.body.message.toString())

                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), "Something went wrong")
                }
            }
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}