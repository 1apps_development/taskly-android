package com.workdo.taskly.activity

import android.content.Intent
import android.view.View
import androidx.activity.viewModels
import androidx.lifecycle.lifecycleScope
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.FirebaseApp
import com.google.firebase.messaging.FirebaseMessaging
import com.workdo.taskly.MainActivity
import com.workdo.taskly.R
import com.workdo.taskly.base.BaseActivity
import com.workdo.taskly.viewModel.AuthViewModel
import com.workdo.taskly.databinding.ActEmailLoginBinding
import com.workdo.taskly.model.LoginData
import com.workdo.taskly.networking.ApiClient
import com.workdo.taskly.remote.NetworkResponse
import com.workdo.taskly.util.BaseResponse
import com.workdo.taskly.util.SharePreference
import com.workdo.taskly.util.Utils
import kotlinx.coroutines.launch

class ActEmailLogin : BaseActivity() {

    private var _binding: ActEmailLoginBinding? = null
    private val viewModel by viewModels<AuthViewModel>()

    private var token=""

    private val binding get() = _binding!!

    override fun setLayout(): View = binding.root

    override fun initView() {
        _binding = ActEmailLoginBinding.inflate(layoutInflater)
        FirebaseApp.initializeApp(this@ActEmailLogin)
        FirebaseMessaging.getInstance().token
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    return@OnCompleteListener
                }
                token = task.result.toString()
            })




        initClickListeners()
    }

    private fun initClickListeners() {
        binding.tvForgotPassword.setOnClickListener {
            startActivity(Intent(this@ActEmailLogin, ActForgotPassword::class.java))
        }

        binding.btnLogin.setOnClickListener {
            if (binding.edEmailAddress.text.toString().isEmpty()) {
                Utils.errorAlert(this@ActEmailLogin, resources.getString(R.string.email_error))
            } else if (binding.edPassword.text.toString().isEmpty()) {
                Utils.errorAlert(this@ActEmailLogin, resources.getString(R.string.password_error))

            } else {

                val request = HashMap<String, String>()
                request["email"] = binding.edEmailAddress.text.toString()
                request["password"] = binding.edPassword.text.toString()
                request["device_type"]="android"
                request["token"]=token
                callLoginApi(request)
            }


        }
    }


    private fun callLoginApi(loginRequest: HashMap<String, String>) {
        Utils.showLoadingProgress(this@ActEmailLogin)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActEmailLogin).getLogin(loginRequest)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val loginResponse = response.body.data

                    when (response.body.status) {
                        1 -> {

                            SharePreference.setStringPref(this@ActEmailLogin,
                                SharePreference.token,
                                loginResponse?.accessToken.toString())
                            SharePreference.setStringPref(this@ActEmailLogin,
                                SharePreference.userId,
                                loginResponse?.id.toString())
                            SharePreference.setStringPref(this@ActEmailLogin,
                                SharePreference.userName,
                                loginResponse?.name.toString())
                            SharePreference.setStringPref(this@ActEmailLogin,
                                SharePreference.userEmail,
                                loginResponse?.email.toString())
                            SharePreference.setStringPref(this@ActEmailLogin,
                                SharePreference.userProfile,
                                loginResponse?.imageUrl.toString())

                            startActivity(Intent(this@ActEmailLogin, MainActivity::class.java))
                            finish()
                        }

                        0 -> {
                            Utils.errorAlert(this@ActEmailLogin, response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActEmailLogin, response.body.message.toString())

                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActEmailLogin, resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActEmailLogin, "Something went wrong")
                }
            }
        }
    }


}