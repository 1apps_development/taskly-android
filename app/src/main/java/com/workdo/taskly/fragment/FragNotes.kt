package com.workdo.taskly.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.workdo.taskly.R
import com.workdo.taskly.adapter.NotesAdapter
import com.workdo.taskly.databinding.FragNotesBinding
import com.workdo.taskly.databinding.NewNotesDialogBinding
import com.workdo.taskly.databinding.NewUserDialogBinding
import com.workdo.taskly.databinding.NotesDetailDialogBinding
import com.workdo.taskly.util.Common
import com.workdo.taskly.util.Constants


class FragNotes : Fragment() {
    private lateinit var binding: FragNotesBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragNotesBinding.inflate(layoutInflater)
        setupNotesAdapter()
        initClickListeners()
        return binding.root
    }

    private fun setupNotesAdapter() {
        binding.rvNotes.apply {
            layoutManager = LinearLayoutManager(requireActivity())
            adapter = NotesAdapter{s:String,i:Int->
                if(s==Constants.ItemClick)
                {
                    notesDetailDialog()
                }

            }
        }

        binding.rvSharedNotes.apply {

            layoutManager = LinearLayoutManager(requireActivity())
            adapter = NotesAdapter{ s:String,i:Int->
                if(s==Constants.ItemClick)
                {
                    notesDetailDialog()
                }
            }
        }
    }

    private fun initClickListeners() {


        binding.btnAddNewNote.setOnClickListener {
            notesDialog()
        }

        binding.btnAddSharedNote.setOnClickListener {
            notesDialog()
        }


    }


    private fun notesDialog() {
        val bottomSheetDialog = BottomSheetDialog(requireActivity(), R.style.BottomSheetDialogTheme)
        bottomSheetDialog.setOnShowListener {
            val bottomSheet: FrameLayout = bottomSheetDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet) ?: return@setOnShowListener
            val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
            if (bottomSheet.layoutParams != null) { Common.showFullScreenBottomSheet(bottomSheet) }

            bottomSheet.setBackgroundResource(android.R.color.transparent)
            Common.expandBottomSheet(bottomSheetBehavior)
        }

        val bottomSheetBinding = NewNotesDialogBinding.inflate(layoutInflater)
        bottomSheetDialog.setContentView(bottomSheetBinding.root)

        bottomSheetBinding.tvClose.setOnClickListener {
            bottomSheetDialog.dismiss()
        }

        bottomSheetDialog.show()
    }


    private fun notesDetailDialog() {
        val bottomSheetDialog = BottomSheetDialog(requireActivity(), R.style.BottomSheetDialogTheme)
        bottomSheetDialog.setOnShowListener {
            val bottomSheet: FrameLayout = bottomSheetDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet) ?: return@setOnShowListener
            val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
            if (bottomSheet.layoutParams != null) { Common.showFullScreenBottomSheet(bottomSheet) }

            bottomSheet.setBackgroundResource(android.R.color.transparent)
            Common.expandBottomSheet(bottomSheetBehavior)
        }

        val bottomSheetBinding = NotesDetailDialogBinding.inflate(layoutInflater)
        bottomSheetDialog.setContentView(bottomSheetBinding.root)

        bottomSheetBinding.tvClose.setOnClickListener {
            bottomSheetDialog.dismiss()
        }

        bottomSheetDialog.show()
    }


}