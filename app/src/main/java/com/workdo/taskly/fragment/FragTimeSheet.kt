package com.workdo.taskly.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.workdo.taskly.R
import com.workdo.taskly.adapter.TimeSheetAdapter
import com.workdo.taskly.databinding.FragTimeSheetBinding


class FragTimeSheet : Fragment() {
    private lateinit var binding: FragTimeSheetBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragTimeSheetBinding.inflate(layoutInflater)
        setupAdapter()
        return binding.root
    }


    private fun setupAdapter() {
        binding.rvTimeList.apply {
            layoutManager = LinearLayoutManager(requireActivity())
            adapter = TimeSheetAdapter()
            isNestedScrollingEnabled = true
        }
    }

}