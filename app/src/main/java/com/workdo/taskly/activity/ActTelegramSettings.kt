package com.workdo.taskly.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.workdo.taskly.R
import com.workdo.taskly.databinding.ActTelegramSettingsBinding

class ActTelegramSettings : AppCompatActivity() {

    private lateinit var binding:ActTelegramSettingsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActTelegramSettingsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.ivBack.setOnClickListener {
            finish()
        }
    }
}