package com.workdo.taskly.model

import com.google.gson.annotations.SerializedName

data class UserListResponse(

	@field:SerializedName("data")
	val data: UserData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class StagesItem(

	@field:SerializedName("stage")
	val stage: String? = null,

	@field:SerializedName("count")
	val count: Int? = null
)


data class UserData(

	@field:SerializedName("per_page")
	val perPage: Int? = null,

	@field:SerializedName("data")
	val data: ArrayList<UserDataItem>? = null,

	@field:SerializedName("last_page")
	val lastPage: Int? = null,

	@field:SerializedName("next_page_url")
	val nextPageUrl: String? = null,

	@field:SerializedName("prev_page_url")
	val prevPageUrl: Any? = null,

	@field:SerializedName("first_page_url")
	val firstPageUrl: String? = null,

	@field:SerializedName("path")
	val path: String? = null,

	@field:SerializedName("total")
	val total: Int? = null,

	@field:SerializedName("last_page_url")
	val lastPageUrl: String? = null,

	@field:SerializedName("from")
	val from: Int? = null,

	@field:SerializedName("links")
	val links: ArrayList<LinksItem>? = null,

	@field:SerializedName("to")
	val to: Int? = null,

	@field:SerializedName("current_page")
	val currentPage: Int? = null
)

data class UserDataItem(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("total_tasks")
	val totalTasks: Int? = null,

	@field:SerializedName("stages")
	val stages: ArrayList<StagesItem>? = null,

	@field:SerializedName("total_projects")
	val totalProjects: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("avatar")
	val avatar: String? = null,

	@field:SerializedName("progress_bar")
	val progressBar: Int? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("email")
	val email: String? = null
)
