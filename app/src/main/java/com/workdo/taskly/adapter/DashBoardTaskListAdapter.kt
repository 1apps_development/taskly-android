package com.workdo.taskly.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.workdo.taskly.databinding.CellTaskListBinding
import com.workdo.taskly.model.DashboardTasksDataItem
import com.workdo.taskly.util.Constants

class DashBoardTaskListAdapter(
    private val taskList: ArrayList<DashboardTasksDataItem>,
    private val listener: (String, Int) -> Unit,
) : RecyclerView.Adapter<DashBoardTaskListAdapter.DefaultViewHolder>() {

    inner class DefaultViewHolder(var itemBinding: CellTaskListBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bindItems(position: Int, listener: (String, Int) -> Unit) = with(itemBinding)
        {

            itemView.setOnClickListener {
                listener(Constants.ItemClick, position)
            }
            tvTaskCategory.text=taskList[position].title.toString()
            tvPriority.text=taskList[position].priority.toString()
            tvTaskDescription.text=taskList[position].description.toString()
            tvTaskName.text=taskList[position].projectName.toString()

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DefaultViewHolder {
        val view = CellTaskListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return DefaultViewHolder(view)
    }

    override fun onBindViewHolder(holder: DefaultViewHolder, position: Int) {
        holder.bindItems(position, listener)

    }

    override fun getItemCount(): Int {
        return taskList.size
    }
}