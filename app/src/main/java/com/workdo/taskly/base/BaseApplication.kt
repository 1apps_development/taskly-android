package com.workdo.taskly.base


import android.app.Application
import androidx.multidex.MultiDex
import com.workdo.taskly.R
import io.github.inflationx.calligraphy3.CalligraphyConfig
import io.github.inflationx.calligraphy3.CalligraphyInterceptor
import io.github.inflationx.viewpump.ViewPump


class BaseApplication : Application() {

    companion object {
        lateinit var app: BaseApplication



        fun getInstance(): BaseApplication {
            return app
        }
    }
    override fun onCreate() {
        super.onCreate()

        app = this
        MultiDex.install(this)

    }
}