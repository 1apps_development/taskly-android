package com.workdo.taskly.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import com.workdo.taskly.MainActivity
import com.workdo.taskly.R
import com.workdo.taskly.base.BaseActivity
import com.workdo.taskly.databinding.ActSplashBinding
import com.workdo.taskly.util.SharePreference

class ActSplash : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.act_splash)
        val userId =
            SharePreference.getStringPref(this@ActSplash, SharePreference.userId).toString()
        Handler(Looper.getMainLooper()).postDelayed({

            if (userId.isNotEmpty()) {
                val intent = Intent(this@ActSplash, MainActivity::class.java)
                startActivity(intent)
            }else
            {
                startActivity(Intent(this@ActSplash, ActIntro::class.java))

            }
            finish()
        }, 2000)

    }

}