package com.workdo.taskly.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.workdo.taskly.base.BaseActivity
import com.workdo.taskly.databinding.ActOtpBinding

class ActOtp : BaseActivity() {
    private lateinit var binding: ActOtpBinding

    override fun setLayout(): View = binding.root

    override fun initView() {

        binding = ActOtpBinding.inflate(layoutInflater)

        binding.btnSendCode.setOnClickListener {
            startActivity(Intent(this@ActOtp, ActChangePasswordSuccess::class.java))
        }
    }
}