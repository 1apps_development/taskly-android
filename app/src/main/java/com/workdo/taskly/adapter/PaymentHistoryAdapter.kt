package com.workdo.taskly.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.workdo.taskly.databinding.CellPaymentHistoryBinding

class PaymentHistoryAdapter :RecyclerView.Adapter<PaymentHistoryAdapter.PaymentHistoryViewHolder>() {

    inner  class PaymentHistoryViewHolder(itemBinding:CellPaymentHistoryBinding):RecyclerView.ViewHolder(itemBinding.root)
    {


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentHistoryViewHolder {
        val view= CellPaymentHistoryBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return PaymentHistoryViewHolder(view)
    }

    override fun onBindViewHolder(holder: PaymentHistoryViewHolder, position: Int) {
    }

    override fun getItemCount(): Int {

        return 3
    }
}