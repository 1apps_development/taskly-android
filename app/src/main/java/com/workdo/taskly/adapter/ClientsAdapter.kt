package com.workdo.taskly.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.workdo.taskly.databinding.CellClientsBinding
import com.workdo.taskly.model.ClientDataItem
import com.workdo.taskly.util.ExtensionFunctions.loadUrlCircle

class ClientsAdapter(var clientList: ArrayList<ClientDataItem>) :
    RecyclerView.Adapter<ClientsAdapter.ClientsViewHolder>() {

    inner class ClientsViewHolder(var itemBinding: CellClientsBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bindItems(data: ClientDataItem) = with(itemBinding)
        {
            ivClientProfile.loadUrlCircle(data.avatar)
            tvClientsName.text = data.name.toString()
            tvEmail.text = data.email.toString()
            tvProjects.text = data.totalProject.toString().plus(" Projects")
            tvTasks.text = data.totalTask.toString().plus(" Tasks")
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClientsViewHolder {
        val view = CellClientsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ClientsViewHolder(view)
    }

    override fun onBindViewHolder(holder: ClientsViewHolder, position: Int) {
        holder.bindItems(clientList[position])
    }

    override fun getItemCount(): Int {
        return clientList.size
    }
}