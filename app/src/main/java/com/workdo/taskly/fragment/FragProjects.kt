package com.workdo.taskly.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.workdo.taskly.R
import com.workdo.taskly.adapter.OrderSummeryAdapter
import com.workdo.taskly.adapter.UsersAdapter
import com.workdo.taskly.databinding.FragProjectsBinding
import com.workdo.taskly.databinding.InvoiceDetailDialogBinding
import com.workdo.taskly.databinding.NewProjectDialogBinding
import com.workdo.taskly.databinding.ProjectDetailBinding
import com.workdo.taskly.model.UserDataItem
import com.workdo.taskly.util.Common


class FragProjects : Fragment() {
    private  var _binding: FragProjectsBinding?=null
    private var userList=ArrayList<UserDataItem>()
    private val binding get()=_binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragProjectsBinding.inflate(layoutInflater)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpAdapter()
        initClickListeners()
    }

    private fun setUpAdapter() {
        binding.rvUsers.apply {
            layoutManager = LinearLayoutManager(requireActivity())
            adapter = UsersAdapter(userList)
            isNestedScrollingEnabled = true
        }


    }

    private fun initClickListeners() {
        binding.constraintProjectDetail.setOnClickListener {
            projectDetailDialog()
        }

        binding.btnAddNewProject.setOnClickListener {
            newProjectDialog()
        }
    }


    private fun projectDetailDialog() {
        val bottomSheetDialog = BottomSheetDialog(requireActivity(), R.style.BottomSheetDialogTheme)
        bottomSheetDialog.setOnShowListener {
            val bottomSheet: FrameLayout =
                bottomSheetDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet)
                    ?: return@setOnShowListener
            val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
            if (bottomSheet.layoutParams != null) {
                Common.showFullScreenBottomSheet(bottomSheet)
            }

            bottomSheet.setBackgroundResource(android.R.color.transparent)
            Common.expandBottomSheet(bottomSheetBehavior)
        }

        val bottomSheetBinding = ProjectDetailBinding.inflate(layoutInflater)
        bottomSheetDialog.setContentView(bottomSheetBinding.root)

        bottomSheetBinding.tvClose.setOnClickListener {
            bottomSheetDialog.dismiss()
        }

        bottomSheetDialog.show()

    }

    private fun newProjectDialog() {
        val bottomSheetDialog = BottomSheetDialog(requireActivity(), R.style.BottomSheetDialogTheme)
        bottomSheetDialog.setOnShowListener {
            val bottomSheet: FrameLayout =
                bottomSheetDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet)
                    ?: return@setOnShowListener
            val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
            if (bottomSheet.layoutParams != null) {
                Common.showFullScreenBottomSheet(bottomSheet)
            }

            bottomSheet.setBackgroundResource(android.R.color.transparent)
            Common.expandBottomSheet(bottomSheetBehavior)
        }

        val bottomSheetBinding = NewProjectDialogBinding.inflate(layoutInflater)
        bottomSheetDialog.setContentView(bottomSheetBinding.root)

        bottomSheetBinding.tvClose.setOnClickListener {
            bottomSheetDialog.dismiss()
        }

        bottomSheetDialog.show()

    }


}