package com.workdo.taskly.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.workdo.taskly.databinding.CellColorBinding

class ColorAdapter:RecyclerView.Adapter<ColorAdapter.ColorViewHolder>() {
    inner class ColorViewHolder(itemBinding:CellColorBinding):RecyclerView.ViewHolder(itemBinding.root)
    {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ColorViewHolder {
        val view=CellColorBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return ColorViewHolder(view)
    }

    override fun onBindViewHolder(holder: ColorViewHolder, position: Int) {
    }

    override fun getItemCount(): Int {
        return 10
    }
}