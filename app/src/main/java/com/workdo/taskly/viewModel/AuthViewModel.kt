package com.workdo.taskly.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.workdo.taskly.Repository
import com.workdo.taskly.model.*
import com.workdo.taskly.util.BaseResponse
import kotlinx.coroutines.launch

class AuthViewModel(application: Application) : AndroidViewModel(application) {

/*    private val userRepo = Repository()
    val loginResult: MutableLiveData<BaseResponse<LoginData>> = MutableLiveData()
     val registerResult: MutableLiveData<BaseResponse<RegisterData>> = MutableLiveData()
     val logoutResult: MutableLiveData<BaseResponse<SingleResponse>> = MutableLiveData()

    fun loginUser(request: HashMap<String, String>) {

        loginResult.value = BaseResponse.Loading()
        viewModelScope.launch {
            try {
                userRepo.loginUser(request).onSuccess {
                    if(it.status==1)
                    {
                        loginResult.value = BaseResponse.Success(it.data)

                    }else
                    {
                        loginResult.value = BaseResponse.Error(it.message)
                    }

                }.onFailure {
                    loginResult.value = BaseResponse.Error(it.localizedMessage)
                }

            } catch (ex: Exception) {
                loginResult.value = BaseResponse.Error(ex.message)
            }

        }
    }

    fun registerUser(request: HashMap<String, String>) {

        viewModelScope.launch {
            userRepo.registerUser(request).onSuccess {
                if(it.status==1)
                {
                    registerResult.value = BaseResponse.Success(it.data)
                }else
                {
                    registerResult.value = BaseResponse.Error(it.message)
                }

            }.onFailure {
                registerResult.value = BaseResponse.Error(it.message)
            }

        }


    }

    fun logOut()
    {
        viewModelScope.launch {
            userRepo.logOut().onSuccess {
                if(it.status==1)
                {
                    registerResult.value = BaseResponse.Success()
                } else
                {
                    logoutResult.value = BaseResponse.Error(it.message)
                }
            }.onFailure {
                logoutResult.value = BaseResponse.Error(it.message)
            }
        }
    }*/

}
