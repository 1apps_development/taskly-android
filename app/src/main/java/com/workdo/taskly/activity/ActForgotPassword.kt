package com.workdo.taskly.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.workdo.taskly.base.BaseActivity
import com.workdo.taskly.databinding.ActForgotPasswordBinding

class ActForgotPassword : BaseActivity() {
    private lateinit var binding:ActForgotPasswordBinding

    override fun setLayout(): View =binding.root

    override fun initView() {
        binding= ActForgotPasswordBinding.inflate(layoutInflater)

        binding.btnSendCode.setOnClickListener {
            startActivity(Intent(this@ActForgotPassword,ActOtp::class.java))
        }
    }

}