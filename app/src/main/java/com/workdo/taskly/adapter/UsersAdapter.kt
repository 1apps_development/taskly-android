package com.workdo.taskly.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.workdo.taskly.databinding.CellUsersBinding
import com.workdo.taskly.model.UserDataItem

class UsersAdapter(private val userList: ArrayList<UserDataItem>) :
    RecyclerView.Adapter<UsersAdapter.UsersViewHolder>() {

    inner class UsersViewHolder(private val itemBinding: CellUsersBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bindItems(dataItem: UserDataItem) = with(itemBinding)
        {
            tvUserProfileName.text=dataItem.name.toString()
            tvEmail.text=dataItem.email.toString()
            tvProjects.text=dataItem.totalProjects.toString().plus(" Projects")
            tvSubTasks.text=dataItem.totalProjects.toString().plus(" Tasks")
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UsersViewHolder {
        val view = CellUsersBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return UsersViewHolder(view)

    }

    override fun onBindViewHolder(holder: UsersViewHolder, position: Int) {
        holder.bindItems(userList[position])
    }

    override fun getItemCount(): Int {
        return userList.size

    }
}