package com.workdo.taskly.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.linroid.filtermenu.library.FilterMenu
import com.linroid.filtermenu.library.FilterMenu.OnMenuChangeListener
import com.linroid.filtermenu.library.FilterMenuLayout
import com.workdo.taskly.MainActivity
import com.workdo.taskly.R
import com.workdo.taskly.adapter.DashBoardTaskListAdapter
import com.workdo.taskly.adapter.SummeryTaskAdapter
import com.workdo.taskly.databinding.FragDashBoardBinding
import com.workdo.taskly.databinding.ProjectDetailBinding
import com.workdo.taskly.model.DashBoardData
import com.workdo.taskly.model.DashboardTasksDataItem
import com.workdo.taskly.networking.ApiClient
import com.workdo.taskly.remote.NetworkResponse
import com.workdo.taskly.util.Common
import com.workdo.taskly.util.Constants
import com.workdo.taskly.util.ExtensionFunctions.loadUrlCircle
import com.workdo.taskly.util.SharePreference
import com.workdo.taskly.util.Utils
import kotlinx.coroutines.launch


class FragDashBoard : Fragment() {
    private var _binding: FragDashBoardBinding? = null
    private val binding get() = _binding!!
    private var taskList=ArrayList<DashboardTasksDataItem>()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {

        _binding = FragDashBoardBinding.inflate(layoutInflater)

        return binding.root
    }


    private fun initData() {

        attachMenu(binding.filterMenu)

        binding.tvUsername.text= "Hello  ${SharePreference.getStringPref(requireActivity(),SharePreference.userName)?:""}"

        binding.ivProfile.loadUrlCircle(SharePreference.getStringPref(requireActivity(),SharePreference.userProfile))

        callDashBoardApi("month")

        binding.rvSummeryTask.apply {
            layoutManager = LinearLayoutManager(requireActivity(), RecyclerView.VERTICAL, false)
            adapter = SummeryTaskAdapter()
            isNestedScrollingEnabled = true
            itemAnimator = DefaultItemAnimator()
        }


        binding.btnTodayTask.setOnClickListener {
            filter("today")
            callDashBoardApi("today")
        }
        binding.btnMonth.setOnClickListener {
            filter("month")
            callDashBoardApi("month")
        }
        binding.btnWeek.setOnClickListener {
            filter("week")
            callDashBoardApi("week")
        }


    }


    private fun callDashBoardApi(taskType: String) {
        Utils.showLoadingProgress(requireActivity())
        val request = HashMap<String, String>()
        request["task_type"] = taskType+

        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity()).dashBoard(request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()

                    when (response.body.status) {
                        1 -> {

                            response.body.data?.let { setupData(it) }
                        }

                        0 -> {
                            Utils.errorAlert(requireActivity(), response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), response.body.message.toString())

                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(),
                        resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), "Something went wrong")
                }
            }
        }


    }


    private fun openBottomSheetDialog() {
        val bottomSheetDialog = BottomSheetDialog(requireActivity(), R.style.BottomSheetDialogTheme)

        bottomSheetDialog.setOnShowListener {
            val bottomSheet: FrameLayout =
                bottomSheetDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet)
                    ?: return@setOnShowListener
            val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
            if (bottomSheet.layoutParams != null) {
                Common.showFullScreenBottomSheet(bottomSheet)
            }

            bottomSheet.setBackgroundResource(android.R.color.transparent)
            Common.expandBottomSheet(bottomSheetBehavior)
        }
        val bottomSheetBinding = ProjectDetailBinding.inflate(layoutInflater)
        bottomSheetDialog.setContentView(bottomSheetBinding.root)
        bottomSheetBinding.btnClose.setOnClickListener {
            bottomSheetDialog.dismiss()
        }
        bottomSheetBinding.tvClose.setOnClickListener {
            bottomSheetDialog.dismiss()
        }

        bottomSheetDialog.show()

    }


    private val listener: OnMenuChangeListener = object : OnMenuChangeListener {
        override fun onMenuItemClick(view: View, position: Int) {
            Toast.makeText(requireActivity(), "Touched position $position", Toast.LENGTH_SHORT)
                .show()

            if (position == 3) {
                val fragment =
                    requireActivity().supportFragmentManager.findFragmentById(R.id.dashBoardContainer)

                (requireActivity() as MainActivity).setAppBarTitle(resources.getString(R.string.tracker))
                if (fragment !is FragTracker) {
                    Common.replaceFragment(
                        requireActivity().supportFragmentManager,
                        FragTracker(),
                        R.id.fragContainer
                    )
                }
            }
        }


        override fun onMenuCollapse() {

        }

        override fun onMenuExpand() {

        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initData()

    }

    private fun setupData(data: DashBoardData) {
        binding.tvTotalClients.text=data.totalMembers.toString().plus(" Clients")
        binding.tvTotalProjects.text=data.totalProject.toString().plus(" Projects")
        binding.tvTotalTask.text=data.totalTask.toString().plus(" Tasks")

        data.tasks?.data?.let { taskList.addAll(it) }

        binding.rvTaskList.apply {
            layoutManager = LinearLayoutManager(requireActivity(), RecyclerView.VERTICAL, false)
            adapter = DashBoardTaskListAdapter(taskList) { type: String, position: Int ->
                run {
                    if (type == Constants.ItemClick) {
                        openBottomSheetDialog()
                    }
                }

            }
            isNestedScrollingEnabled = true
            itemAnimator = DefaultItemAnimator()
        }
    }

    private fun attachMenu(layout: FilterMenuLayout): FilterMenu? {
        return FilterMenu.Builder(requireActivity())
            .addItem(R.drawable.ic_menu_projects)
            .addItem(R.drawable.ic_menu_invoices)
            .addItem(R.drawable.ic_menu_tasks)
            .addItem(R.drawable.ic_tracker)
            .attach(layout)
            .withListener(listener)
            .build()
    }


    private fun filter(type:String)
    {
        when (type) {
            "today" -> {
                binding.btnMonth.background=ResourcesCompat.getDrawable(resources,R.drawable.round_gray_stroke_45,null)
                binding.btnTodayTask.background=ResourcesCompat.getDrawable(resources,R.drawable.green_round_fill_35,null)
                binding.btnWeek.background=ResourcesCompat.getDrawable(resources,R.drawable.round_gray_stroke_45,null)

                binding.btnWeek.setTextColor(ResourcesCompat.getColor(resources,R.color.gray,null))
                binding.btnTodayTask.setTextColor(ResourcesCompat.getColor(resources,R.color.white,null))
                binding.btnMonth.setTextColor(ResourcesCompat.getColor(resources,R.color.gray,null))
            }
            "week" -> {
                binding.btnMonth.background=ResourcesCompat.getDrawable(resources,R.drawable.round_gray_stroke_45,null)
                binding.btnTodayTask.background=ResourcesCompat.getDrawable(resources,R.drawable.round_gray_stroke_45,null)
                binding.btnWeek.background=ResourcesCompat.getDrawable(resources,R.drawable.green_round_fill_35,null)


                binding.btnTodayTask.setTextColor(ResourcesCompat.getColor(resources,R.color.gray,null))
                binding.btnWeek.setTextColor(ResourcesCompat.getColor(resources,R.color.white,null))
                binding.btnMonth.setTextColor(ResourcesCompat.getColor(resources,R.color.gray,null))
            }
            "month" -> {
                binding.btnMonth.background=ResourcesCompat.getDrawable(resources,R.drawable.green_round_fill_35,null)
                binding.btnTodayTask.background=ResourcesCompat.getDrawable(resources,R.drawable.round_gray_stroke_45,null)
                binding.btnWeek.background=ResourcesCompat.getDrawable(resources,R.drawable.round_gray_stroke_45,null)


                binding.btnTodayTask.setTextColor(ResourcesCompat.getColor(resources,R.color.gray,null))
                binding.btnMonth.setTextColor(ResourcesCompat.getColor(resources,R.color.white,null))
                binding.btnWeek.setTextColor(ResourcesCompat.getColor(resources,R.color.gray,null))
            }
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}