package com.workdo.taskly.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.workdo.taskly.databinding.ActRegisterOptionBinding

class ActRegisterOption : AppCompatActivity() {
    private  var _binding:ActRegisterOptionBinding?=null
    private val binding get()=_binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding= ActRegisterOptionBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initClickListeners()
    }


    private fun initClickListeners()
    {
        binding.linearEmail.setOnClickListener {
            startActivity(Intent(this@ActRegisterOption,ActSignup::class.java))
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding=null
    }
}