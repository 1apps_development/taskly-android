package com.workdo.taskly.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.workdo.taskly.databinding.CellOrderSummeryBinding

class OrderSummeryAdapter : RecyclerView.Adapter<OrderSummeryAdapter.SummeryViewHolder>() {
    inner class SummeryViewHolder(itemBinding: CellOrderSummeryBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SummeryViewHolder {
        val view = CellOrderSummeryBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SummeryViewHolder(view)

    }

    override fun onBindViewHolder(holder: SummeryViewHolder, position: Int) {

    }

    override fun getItemCount(): Int {
        return 5
    }
}