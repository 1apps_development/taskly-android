package com.workdo.taskly.activity

import android.content.Intent
import android.view.View
import com.workdo.taskly.base.BaseActivity
import com.workdo.taskly.databinding.ActLoginBinding

class ActLogin : BaseActivity() {

    private var _binding: ActLoginBinding? = null
    private val binding get() = _binding!!

    override fun setLayout(): View = binding.root

    override fun initView() {
        _binding = ActLoginBinding.inflate(layoutInflater)

        binding.linearEmail.setOnClickListener {
            startActivity(Intent(this@ActLogin, ActEmailLogin::class.java))
        }





    }





    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}