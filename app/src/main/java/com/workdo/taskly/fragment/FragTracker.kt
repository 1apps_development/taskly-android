package com.workdo.taskly.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.workdo.taskly.R
import com.workdo.taskly.adapter.TrackerAdapter
import com.workdo.taskly.databinding.ActTaxesBinding
import com.workdo.taskly.databinding.FragTrackerBinding


class FragTracker : Fragment() {
    private lateinit var binding:FragTrackerBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
         binding= FragTrackerBinding.inflate(layoutInflater)
        setupAdapter()
        return binding.root
    }


    private fun setupAdapter()
    {
        binding.rvTrackers.apply {
            layoutManager=LinearLayoutManager(requireActivity())
            adapter=TrackerAdapter()
        }
    }


}