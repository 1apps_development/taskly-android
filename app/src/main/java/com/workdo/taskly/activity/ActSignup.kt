package com.workdo.taskly.activity

import android.content.Intent
import android.view.View
import androidx.activity.viewModels
import androidx.lifecycle.lifecycleScope
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.FirebaseApp
import com.google.firebase.messaging.FirebaseMessaging
import com.workdo.taskly.MainActivity
import com.workdo.taskly.R
import com.workdo.taskly.base.BaseActivity
import com.workdo.taskly.viewModel.AuthViewModel
import com.workdo.taskly.databinding.ActSignupBinding
import com.workdo.taskly.model.RegisterData
import com.workdo.taskly.networking.ApiClient
import com.workdo.taskly.remote.NetworkResponse
import com.workdo.taskly.util.BaseResponse
import com.workdo.taskly.util.SharePreference
import com.workdo.taskly.util.Utils
import kotlinx.coroutines.launch

class ActSignup : BaseActivity() {
    private var _binding: ActSignupBinding? = null
    private val viewModel by viewModels<AuthViewModel>()
    private var token = ""

    private val binding get() = _binding!!

    override fun setLayout(): View = binding.root

    override fun initView() {
        _binding = ActSignupBinding.inflate(layoutInflater)

        FirebaseApp.initializeApp(this@ActSignup)
        FirebaseMessaging.getInstance().token
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    return@OnCompleteListener
                }
                token = task.result.toString()
            })
        binding.btnSignUp.setOnClickListener {

            validation()
        }


    }


    private fun loginData(data: RegisterData?) {
        SharePreference.setStringPref(this@ActSignup,
            SharePreference.token,
            data?.tokenType.toString())
        SharePreference.setStringPref(this@ActSignup,
            SharePreference.userId,
            data?.id.toString())
        SharePreference.setStringPref(this@ActSignup,
            SharePreference.userName,
            data?.name.toString())
        SharePreference.setStringPref(this@ActSignup,
            SharePreference.userEmail,
            data?.email.toString())
        SharePreference.setStringPref(this@ActSignup,
            SharePreference.userProfile,
            data?.imageUrl.toString())

        startActivity(Intent(this@ActSignup, ActLogin::class.java))

    }


    private fun validation() {
        if (binding.edFullname.text.toString().isEmpty()) {

            Utils.errorAlert(this@ActSignup, "Name is  required")

        } else if (binding.edEmailAddress.text.toString().isEmpty()) {
            Utils.errorAlert(this@ActSignup, "Email is  required")


        } else if (binding.edWorkSpace.text.toString().isEmpty()) {
            Utils.errorAlert(this@ActSignup, "Workspace is  required")

        } else if (binding.edPassword.text.toString().isEmpty()) {
            Utils.errorAlert(this@ActSignup, "password is  required")
        } else {
            val request = HashMap<String, String>()
            request["email"] = binding.edEmailAddress.text.toString()
            request["password"] = binding.edPassword.text.toString()
            request["workspace"] = binding.edWorkSpace.text.toString()
            request["name"] = binding.edWorkSpace.text.toString()
            request["device_type"] = "android"
            request["register_type"] = "email"
            request["token"] = token
            callRegisterApi(request)
        }
    }
    private fun callRegisterApi(loginRequest: HashMap<String, String>) {
        Utils.showLoadingProgress(this@ActSignup)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActSignup).register(loginRequest)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val dataResponse = response.body.data

                    when (response.body.status) {
                        1 -> {

                            SharePreference.setStringPref(this@ActSignup,
                                SharePreference.token,
                                dataResponse?.tokenType.toString())
                            SharePreference.setStringPref(this@ActSignup,
                                SharePreference.userId,
                                dataResponse?.id.toString())
                            SharePreference.setStringPref(this@ActSignup,
                                SharePreference.userName,
                                dataResponse?.name.toString())
                            SharePreference.setStringPref(this@ActSignup,
                                SharePreference.userEmail,
                                dataResponse?.email.toString())
                            SharePreference.setStringPref(this@ActSignup,
                                SharePreference.userProfile,
                                dataResponse?.imageUrl.toString())

                            startActivity(Intent(this@ActSignup, ActLogin::class.java))

                            startActivity(Intent(this@ActSignup, MainActivity::class.java))
                            finish()
                        }

                        0 -> {
                                Utils.errorAlert(this@ActSignup, response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActSignup, response.body.message.toString())

                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActSignup, resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActSignup, "Something went wrong")
                }
            }
        }
    }




    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}