package com.workdo.taskly.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.workdo.taskly.databinding.FragCalenderBinding
import java.util.*


class FragCalender : Fragment() {

    private lateinit var binding:FragCalenderBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    @SuppressLint("ResourceAsColor")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding= FragCalenderBinding.inflate(layoutInflater)
        val min=Calendar.getInstance()

        binding.calendarView.date = System.currentTimeMillis()


        return binding.root
    }

}