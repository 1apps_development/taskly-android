package com.workdo.taskly.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.workdo.taskly.adapter.ColorAdapter
import com.workdo.taskly.adapter.OrderSummeryAdapter
import com.workdo.taskly.databinding.ActInvoiceSettingBinding

class ActInvoiceSetting : AppCompatActivity() {
    private lateinit var binding: ActInvoiceSettingBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActInvoiceSettingBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initClickListener()

        setupAdapter()
    }

    private fun initClickListener() {
        binding.ivBack.setOnClickListener {
            finish()
        }

    }

    private  fun setupAdapter()
    {
        binding.rvColors.apply {
            layoutManager=GridLayoutManager(this@ActInvoiceSetting,6,RecyclerView.VERTICAL,false)
            adapter=ColorAdapter()
            isNestedScrollingEnabled=true

        }
        binding.rvOrderSummery.apply {
            layoutManager= LinearLayoutManager(this@ActInvoiceSetting)
            adapter= OrderSummeryAdapter()
            isNestedScrollingEnabled=true
        }

    }
}