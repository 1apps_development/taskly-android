package com.workdo.taskly.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.workdo.taskly.R
import com.workdo.taskly.databinding.ActZoomDetailsBinding

class ActZoomDetails : AppCompatActivity() {
    private lateinit var binding:ActZoomDetailsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActZoomDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initClickListeners()
    }

    private fun initClickListeners()
    {
        binding.ivBack.setOnClickListener {
            finish()
        }
    }
}