package com.workdo.taskly.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.workdo.taskly.databinding.CellQuickInvoicesBinding

class QuickInvoiceAdapter:RecyclerView.Adapter<QuickInvoiceAdapter.QuickInvoiceViewHolder>() {

    inner class QuickInvoiceViewHolder(itemBinding:CellQuickInvoicesBinding):RecyclerView.ViewHolder(itemBinding.root)
    {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuickInvoiceViewHolder {

        val view=CellQuickInvoicesBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return QuickInvoiceViewHolder(view)
    }

    override fun onBindViewHolder(holder: QuickInvoiceViewHolder, position: Int) {
    }

    override fun getItemCount(): Int {
   return 3
    }
}