package com.workdo.taskly.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.workdo.taskly.R
import com.workdo.taskly.databinding.FragZoomMeetingsBinding


class FragZoomMeetings : Fragment() {

    private var _binding: FragZoomMeetingsBinding? = null
    private val binding get()=_binding!!
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragZoomMeetingsBinding.inflate(layoutInflater)
        return binding.root
    }


    override fun onDestroy() {
        super.onDestroy()
        _binding=null
    }
}