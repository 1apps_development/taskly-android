package com.workdo.taskly.networking

import android.content.Context
import com.google.gson.GsonBuilder
import com.workdo.taskly.base.BaseApplication
import com.workdo.taskly.exception.ResultCallAdapterFactory
import com.workdo.taskly.remote.NetworkResponseAdapterFactory
import com.workdo.taskly.util.AuthInterceptor
import com.workdo.taskly.util.SharePreference
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit

object ApiClient {
    private const val BASE_URL = "https://apps.rajodiya.com/taskly-api/"
    private const val API_URL = BASE_URL + "api/"

    private var TIMEOUT: Long = 60 * 2 * 2.toLong()

    fun getClient(context:Context): ApiInterface {
            val headerInterceptor = Interceptor { chain ->
                var request = chain.request()
                request = request.newBuilder()
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Accept", "application/json")
                    .addHeader("Authorization", "Bearer ${SharePreference.getStringPref(context,SharePreference.token)}")
                    .build()
                val response = chain.proceed(request)
                response
            }
            val logging = HttpLoggingInterceptor()
            logging.setLevel(HttpLoggingInterceptor.Level.BODY)
            val httpClient = OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT, TimeUnit.SECONDS)
            httpClient.addInterceptor(headerInterceptor)
            httpClient.addInterceptor(AuthInterceptor(context))
            httpClient.addInterceptor(logging)
            val gson = GsonBuilder().setLenient().create()
            val retrofit = Retrofit.Builder()
                .baseUrl(API_URL)
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addConverterFactory(ScalarsConverterFactory.create())
                .addCallAdapterFactory(NetworkResponseAdapterFactory())
                .build()
            return retrofit.create(ApiInterface::class.java)
    }

}


