package com.workdo.taskly.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.workdo.taskly.databinding.CellTaskCategoryBinding

class TaskCategoryAdapter :RecyclerView.Adapter<TaskCategoryAdapter.TaskCategoryViewHolder>() {

    inner class TaskCategoryViewHolder(var itemBinding:CellTaskCategoryBinding):RecyclerView.ViewHolder(itemBinding.root)
    {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskCategoryViewHolder {

        val view=CellTaskCategoryBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return TaskCategoryViewHolder(view)
    }

    override fun onBindViewHolder(holder: TaskCategoryViewHolder, position: Int) {
    }

    override fun getItemCount(): Int {
        return 3
    }
}