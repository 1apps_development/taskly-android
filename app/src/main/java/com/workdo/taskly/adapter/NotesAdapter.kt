package com.workdo.taskly.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.workdo.taskly.databinding.CellNotesBinding
import com.workdo.taskly.databinding.FragNotesBinding
import com.workdo.taskly.util.Constants

class NotesAdapter(val listener: (String,Int) -> Unit):RecyclerView.Adapter<NotesAdapter.NotesViewHolder>(){

    inner class NotesViewHolder(itemBinding:CellNotesBinding):RecyclerView.ViewHolder(itemBinding.root)
    {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotesViewHolder {
        val view =CellNotesBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return NotesViewHolder(view)

    }

    override fun onBindViewHolder(holder: NotesViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            listener(Constants.ItemClick,position)
        }
    }

    override fun getItemCount(): Int {
        return 3
    }
}