package com.workdo.taskly.util

import android.Manifest
import android.app.Activity
import android.app.ActivityManager
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.provider.Settings
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.workdo.taskly.R
import com.workdo.taskly.activity.ActLogin
import com.workdo.taskly.util.SharePreference.Companion.setStringPref
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern
import kotlin.collections.ArrayList


object Utils {
    var dialog: Dialog? = null
    fun dismissLoadingProgress() {
        if (dialog != null && dialog!!.isShowing) {
            dialog!!.dismiss()
        }
    }

    fun setRequestBody(bodyData: String): RequestBody {
        return bodyData.toRequestBody("text/plain".toMediaType())
    }



    fun setImageUpload(strParameter: String, mSelectedFileImg: File): MultipartBody.Part {
        return MultipartBody.Part.createFormData(
            strParameter,
            mSelectedFileImg.name,
            mSelectedFileImg.asRequestBody("image/*".toMediaType())
        )
    }


    fun setInvalidToken(activity: Context) {
        val preference = SharePreference(activity)

        preference.mLogout()


        setStringPref(activity, SharePreference.userId, "")



        val intent = Intent(activity, ActLogin::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        activity.startActivity(intent)
//        activity.finish()
    }


    fun postTimeFormat(oldTime: Date): String? {

        val dailyFormat = SimpleDateFormat("hh:mm aa", Locale.getDefault())
        dailyFormat.timeZone = TimeZone.getDefault()
        val date = SimpleDateFormat("MMM dd, yyyy", Locale.getDefault())
        date.timeZone = TimeZone.getDefault()

        val newTime = Date()
        try {
            val cal: Calendar = Calendar.getInstance()
            cal.time = newTime
            val oldCal: Calendar = Calendar.getInstance()
            oldCal.time = oldTime
            val oldYear: Int = oldCal.get(Calendar.YEAR)
            val year: Int = cal.get(Calendar.YEAR)
            val oldDay: Int = oldCal.get(Calendar.DAY_OF_YEAR)
            val day: Int = cal.get(Calendar.DAY_OF_YEAR)
            if (oldYear == year) {
                return when (oldDay - day) {
                    -1 -> {
                        "Yesterday at " + dailyFormat.format(oldTime)
                    }
                    0 -> {
                        "Today at " + dailyFormat.format(oldTime)
                    }
                    else -> {
                        date.format(oldTime)
                    }
                }
            }
        } catch (e: Exception) {
        }
        return date.format(oldTime)
    }


    fun getEventTimeFormat(time: String): String? {
        val timeFormat = SimpleDateFormat("HH:mm:ss", Locale.getDefault())
        val date = timeFormat.parse(time)

        val userTimeFormat = SimpleDateFormat("hh:mm a", Locale.getDefault())
        return userTimeFormat.format(date)
    }

    fun getHourMinuteFormatTime(time: String): String? {
        val timeFormat = SimpleDateFormat("hh:mm:ss", Locale.getDefault())
        val date = timeFormat.parse(time)

        val userTimeFormat = SimpleDateFormat("hh:mm", Locale.getDefault())
        return userTimeFormat.format(date)
    }




    fun getTodayDate(): String {
        val c = Calendar.getInstance().time
        val df = SimpleDateFormat("MM/dd/yyyy", Locale.getDefault())
        return df.format(c)
    }

    fun isForeground(context: Context, PackageName: String): Boolean {
        // Get the Activity Manager
        val manager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager

        // Get a list of running tasks, we are only interested in the last one,
        // the top most so we give a 1 as parameter so we only get the topmost.
        val task = manager.getRunningTasks(1)

        // Get the info we need for comparison.
        val componentInfo = task[0].topActivity

        // Check if it matches our package name.
        return componentInfo?.packageName == PackageName

        // If not then our app is not on the foreground.
    }

    fun getErrorMessage(response: String?): String {
        var errorMessage = "Something went wrong!"
        if (!response.isNullOrEmpty()) {
            val errorObject = JSONObject(response)
            errorMessage = errorObject.getString("message")
        }
        return errorMessage
    }

    fun getFormattedDate(date: String): String {
        val getFormattedDateFormatter = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
        val oldDate = getFormattedDateFormatter.parse(date)
        val newFormatter = SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH)
        return newFormatter.format(oldDate)

    }

/*    fun bitmapFromVector(context: Context, vectorResId: Int): BitmapDescriptor? {
        val vectorDrawable = ContextCompat.getDrawable(context, vectorResId)

        vectorDrawable!!.setBounds(
            0,
            0,
            vectorDrawable.intrinsicWidth,
            vectorDrawable.intrinsicHeight
        )
        val bitmap = Bitmap.createBitmap(
            vectorDrawable.intrinsicWidth,
            vectorDrawable.intrinsicHeight,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)

        vectorDrawable.draw(canvas)

        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }*/
    private fun getRootView(activity: Activity): View? {
        val contentViewGroup = activity.findViewById(android.R.id.content) as ViewGroup?
        var rootView: View? = null
        if (contentViewGroup != null) rootView = contentViewGroup.getChildAt(0)
        if (rootView == null) rootView = activity.window.decorView.getRootView()
        return rootView
    }





    fun checkPermission(context: Context): Boolean {
        val permissionsToRequire = ArrayList<String>()
        if (ContextCompat.checkSelfPermission(
                context,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            permissionsToRequire.add(Manifest.permission.READ_EXTERNAL_STORAGE)
        }
        if (ContextCompat.checkSelfPermission(
                context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            permissionsToRequire.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }

        if (ContextCompat.checkSelfPermission(
                context,
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            permissionsToRequire.add(Manifest.permission.CAMERA)
        }
        if (permissionsToRequire.isNotEmpty()) {
            ActivityCompat.requestPermissions(
                context as Activity,
                permissionsToRequire.toTypedArray(),
                0
            )
            return true
        }
        return false
    }




    fun errorAlert(activity: Activity, message: String) {
        Toast.makeText(activity,message,Toast.LENGTH_SHORT).show()

    }


    fun successAlert(activity: Activity, message: String) {
        Toast.makeText(activity,message,Toast.LENGTH_SHORT).show()
    }





    fun isValidEmail(strPattern: String): Boolean {
        return Pattern.compile(
            "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
        ).matcher(strPattern).matches()
    }


    fun showLoadingProgress(context: Activity?) {
        if (dialog != null) {
            dialog?.dismiss()
            dialog = null
        }
        dialog = context?.let { Dialog(it) }
        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog?.setContentView(R.layout.dlg_progress)
        dialog?.setCancelable(false)
        dialog?.show()
    }

    fun hideKeyboard(activity: Activity) {
        val imm: InputMethodManager =
            activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view: View? = activity.currentFocus
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun showSoftKeyboard(view: View,activity: Activity) {
        if (view.requestFocus()) {
            val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager?
            imm!!.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
        }
    }

    fun showToast(context: Activity?, message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    fun enableLocationPermissionFromSetting(
        title: String,
        message: String,
        context: Context,
        packageName: String
    ) {
        AlertDialog.Builder(context)
            .setTitle(title)
            .setMessage(message)
            .setCancelable(false)
            .setPositiveButton("Settings") { _, _ ->
                val intent = Intent(
                    Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                    Uri.fromParts("package", packageName, null)
                )
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                context.startActivity(intent)
            }.setNegativeButton("Cancel") { dialog, _ ->
                dialog.dismiss()
            }
            .show()
    }
    fun View.errorSnack(message: String, length: Int = Snackbar.LENGTH_LONG) {
        val snack = Snackbar.make(this, message, length)
        snack.setActionTextColor(Color.parseColor("#FFFFFF"))
        snack.view.setBackgroundColor(Color.parseColor("#C62828"))
        snack.show()
    }



}