package com.workdo.taskly.model

import com.google.gson.annotations.SerializedName

data class DashBoardResponse(

	@field:SerializedName("data")
	val data: DashBoardData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class AssignToItem(

	@field:SerializedName("profile_pic")
	val profilePic: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
)

data class Tasks(

	@field:SerializedName("per_page")
	val perPage: Int? = null,

	@field:SerializedName("data")
	val data: ArrayList<DashboardTasksDataItem>? = null,

	@field:SerializedName("last_page")
	val lastPage: Int? = null,

	@field:SerializedName("next_page_url")
	val nextPageUrl: String? = null,

	@field:SerializedName("prev_page_url")
	val prevPageUrl: Any? = null,

	@field:SerializedName("first_page_url")
	val firstPageUrl: String? = null,

	@field:SerializedName("path")
	val path: String? = null,

	@field:SerializedName("total")
	val total: Int? = null,

	@field:SerializedName("last_page_url")
	val lastPageUrl: String? = null,

	@field:SerializedName("from")
	val from: Int? = null,

	@field:SerializedName("links")
	val links: ArrayList<LinksItem>? = null,

	@field:SerializedName("to")
	val to: Int? = null,

	@field:SerializedName("current_page")
	val currentPage: Int? = null
)

data class DashboardTasksDataItem(

	@field:SerializedName("assign_to")
	val assignTo: ArrayList<AssignToItem>? = null,

	@field:SerializedName("milestone_id")
	val milestoneId: String? = null,

	@field:SerializedName("due_date")
	val dueDate: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("priority")
	val priority: String? = null,

	@field:SerializedName("project_name")
	val projectName: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("stage")
	val stage: String? = null,

	@field:SerializedName("project_id")
	val projectId: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("complete")
	val complete: Int? = null,

	@field:SerializedName("start_date")
	val startDate: String? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("order")
	val order: Int? = null
)

data class LinksItem(

	@field:SerializedName("active")
	val active: Boolean? = null,

	@field:SerializedName("label")
	val label: String? = null,

	@field:SerializedName("url")
	val url: Any? = null
)

data class DashBoardData(

	@field:SerializedName("totalProject")
	val totalProject: Int? = null,

	@field:SerializedName("totalMembers")
	val totalMembers: Int? = null,

	@field:SerializedName("totalTask")
	val totalTask: Int? = null,

	@field:SerializedName("tasks")
	val tasks: Tasks? = null
)
