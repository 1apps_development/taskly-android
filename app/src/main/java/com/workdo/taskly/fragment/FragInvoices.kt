package com.workdo.taskly.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.workdo.taskly.R
import com.workdo.taskly.adapter.InvoicesAdapter
import com.workdo.taskly.adapter.NotificationAdapter
import com.workdo.taskly.adapter.OrderSummeryAdapter
import com.workdo.taskly.adapter.QuickInvoiceAdapter
import com.workdo.taskly.databinding.FragInvoicesBinding
import com.workdo.taskly.databinding.InvoiceDetailDialogBinding
import com.workdo.taskly.util.Common
import com.workdo.taskly.util.Constants


class FragInvoices : Fragment() {
    private lateinit var binding: FragInvoicesBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragInvoicesBinding.inflate(layoutInflater)
        setupInvoicesAdapter()
        return binding.root
    }

    private fun setupInvoicesAdapter() {
        binding.rvInvoices.apply {
            layoutManager=LinearLayoutManager(requireActivity())
            adapter=InvoicesAdapter { type: String, pos: Int ->
                run {
                    if (type == Constants.ItemClick) {

                        invoiceDetailDialog()
                    }
                }

            }
            isNestedScrollingEnabled=true
        }

        binding.rvQuickInvoices.apply {
            layoutManager=LinearLayoutManager(requireActivity())
            adapter=QuickInvoiceAdapter()
            isNestedScrollingEnabled=true
        }
    }

    private fun invoiceDetailDialog() {
        val bottomSheetDialog = BottomSheetDialog(requireActivity(), R.style.BottomSheetDialogTheme)
        bottomSheetDialog.setOnShowListener {
            val bottomSheet: FrameLayout = bottomSheetDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet) ?: return@setOnShowListener
            val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
            if (bottomSheet.layoutParams != null) { Common.showFullScreenBottomSheet(bottomSheet) }

            bottomSheet.setBackgroundResource(android.R.color.transparent)
            Common.expandBottomSheet(bottomSheetBehavior)
        }

        val bottomSheetBinding = InvoiceDetailDialogBinding.inflate(layoutInflater)
        bottomSheetDialog.setContentView(bottomSheetBinding.root)

        bottomSheetBinding.rvOrderSummery.apply {
            layoutManager=LinearLayoutManager(requireActivity())
            adapter=OrderSummeryAdapter()
            isNestedScrollingEnabled=true
        }

        bottomSheetBinding.tvClose.setOnClickListener {
            bottomSheetDialog.dismiss()
        }

        bottomSheetDialog.show()

    }


}