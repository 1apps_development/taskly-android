package com.workdo.taskly.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.workdo.taskly.R
import com.workdo.taskly.adapter.NotificationAdapter
import com.workdo.taskly.databinding.FragNotificationsBinding


class FragNotifications : Fragment() {
private lateinit var binding:FragNotificationsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
     }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding= FragNotificationsBinding.inflate(layoutInflater)
        setupAdapter()
        return binding.root
    }
    private fun setupAdapter()
    {
        binding.rvNotifications.apply {
            layoutManager=LinearLayoutManager(requireActivity())
            adapter=NotificationAdapter()
        }
    }



}