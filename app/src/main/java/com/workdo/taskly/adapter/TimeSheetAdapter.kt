package com.workdo.taskly.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.workdo.taskly.databinding.CellTimelistBinding

class TimeSheetAdapter:RecyclerView.Adapter<TimeSheetAdapter.TimeSheetViewHolder>() {

    inner class TimeSheetViewHolder(itemBinding:CellTimelistBinding):RecyclerView.ViewHolder(itemBinding.root)
    {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TimeSheetViewHolder {
        val view=CellTimelistBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return TimeSheetViewHolder(view)
    }

    override fun onBindViewHolder(holder: TimeSheetViewHolder, position: Int) {
    }

    override fun getItemCount(): Int {
    return 3

    }
}