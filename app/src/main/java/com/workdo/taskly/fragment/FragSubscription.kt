package com.workdo.taskly.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.workdo.taskly.adapter.PaymentHistoryAdapter
import com.workdo.taskly.databinding.FragSubsCriptionBinding


class FragSubscription : Fragment() {
    private lateinit var binding: FragSubsCriptionBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragSubsCriptionBinding.inflate(layoutInflater)
        setUpAdapter()
        return binding.root
    }



    private fun setUpAdapter() {
        binding.rvPaymentHistory.apply {
            layoutManager = LinearLayoutManager(requireActivity())
            adapter = PaymentHistoryAdapter()
            isNestedScrollingEnabled = true
        }

    }



}