package com.workdo.taskly.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatImageButton
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import com.workdo.taskly.R
import com.workdo.taskly.databinding.ActTaxesBinding

class ActTaxes : AppCompatActivity() {

    private lateinit var binding: ActTaxesBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActTaxesBinding.inflate(layoutInflater)
        setContentView(binding.root)


        initClickListeners()
        initView()
    }

    private fun initClickListeners() {
        binding.ivBack.setOnClickListener {
            finish()
        }

        binding.btnCreateTax.setOnClickListener {
            addNewView()

        }

    }

    private fun initView()
    {
        for(i  in 0 until 3)
        {
            addNewView()
        }
    }



    private fun addNewView() {
        val inflater = LayoutInflater.from(this@ActTaxes).inflate(R.layout.cell_tax, null)

        val ivDelete: AppCompatImageButton = inflater.findViewById(R.id.ivDelete)



        ivDelete.setOnClickListener {
            onDelete(inflater)

        }

        val layoutParams: LinearLayout.LayoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        layoutParams.topMargin = 12
        inflater.layoutParams = layoutParams
        binding.viewList.addView(inflater, binding.viewList.childCount)

    }

    private fun onDelete(v: View) {

        binding.viewList.removeView(v)
    }
}