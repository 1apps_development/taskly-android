package com.workdo.taskly.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.workdo.taskly.R
import com.workdo.taskly.adapter.ClientsAdapter
import com.workdo.taskly.databinding.FragClientsBinding
import com.workdo.taskly.databinding.NewClientDialogBinding
import com.workdo.taskly.databinding.NewUserDialogBinding
import com.workdo.taskly.model.ClientData
import com.workdo.taskly.model.ClientDataItem
import com.workdo.taskly.networking.ApiClient
import com.workdo.taskly.remote.NetworkResponse
import com.workdo.taskly.util.Utils
import kotlinx.coroutines.launch


class FragClients : Fragment() {
    private var _binding: FragClientsBinding? = null
    private var clientList = ArrayList<ClientDataItem>()
    private lateinit var clientAdapter: ClientsAdapter

    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragClientsBinding.inflate(layoutInflater)
        return binding.root
    }

    private fun setupAdapter() {

        binding.btnAddNewClient.setOnClickListener {

            newClientsDialog()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        clientAdapter = ClientsAdapter(clientList)

        setupAdapter()
        getClientList()
    }

    private fun getClientList() {
        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity()).clientList()) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()

                    when (response.body.status) {
                        1 -> {
                            response.body.data?.data?.let { clientList.addAll(it) }
                            binding.rvClients.apply {
                                layoutManager = LinearLayoutManager(requireActivity())
                                adapter = clientAdapter
                                isNestedScrollingEnabled = true
                            }
                        }

                        0 -> {
                            Utils.errorAlert(requireActivity(), response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), response.body.message.toString())

                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(),
                        resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), "Something went wrong")
                }
            }
        }
    }

    private fun newClientsDialog() {
        val bottomSheetDialog = BottomSheetDialog(requireActivity(), R.style.BottomSheetDialogTheme)


        val bottomSheetBinding = NewClientDialogBinding.inflate(layoutInflater)
        bottomSheetDialog.setContentView(bottomSheetBinding.root)
        bottomSheetBinding.btnClose.setOnClickListener {
            bottomSheetDialog.dismiss()
        }
        bottomSheetBinding.tvClose.setOnClickListener {
            bottomSheetDialog.dismiss()
        }

        bottomSheetBinding.constraintInvite.setOnClickListener {

            if (bottomSheetBinding.edFullname.text.toString().isEmpty()) {
                Utils.errorAlert(requireActivity(), "Name is required")
            } else if (bottomSheetBinding.edEmail.text.toString().isEmpty()) {
                Utils.errorAlert(requireActivity(), "Email is required")

            } else if (bottomSheetBinding.edPassword.text.toString().isEmpty()) {
                Utils.errorAlert(requireActivity(), "Password is required")

            } else {
                val request = HashMap<String, String>()
                request["name"] = bottomSheetBinding.edFullname.text.toString()
                request["email"] = bottomSheetBinding.edEmail.text.toString()
                request["password"] = bottomSheetBinding.edPassword.text.toString()

                createClient(request)
                bottomSheetDialog.dismiss()

            }
        }

        bottomSheetDialog.show()

    }


    private fun createClient(request: HashMap<String, String>) {
        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity()).createClient(request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()

                    when (response.body.status) {
                        1 -> {

                        }

                        0 -> {
                            Utils.errorAlert(requireActivity(), response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), response.body.message.toString())

                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(),
                        resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), "Something went wrong")
                }
            }
        }
    }

}