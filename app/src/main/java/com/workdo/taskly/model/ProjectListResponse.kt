package com.workdo.taskly.model

import com.google.gson.annotations.SerializedName

data class ProjectListResponse(

	@field:SerializedName("data")
	val data: ProjectsData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class Pivot(

	@field:SerializedName("project_id")
	val projectId: Int? = null,

	@field:SerializedName("client_id")
	val clientId: Int? = null,

	@field:SerializedName("user_id")
	val userId: Int? = null,

	@field:SerializedName("workspace_id")
	val workspaceId: Int? = null
)

data class JsonMember2(

	@field:SerializedName("end_date")
	val endDate: String? = null,

	@field:SerializedName("owner_email")
	val ownerEmail: String? = null,

	@field:SerializedName("owner_name")
	val ownerName: String? = null,

	@field:SerializedName("total_tasks")
	val totalTasks: Int? = null,

	@field:SerializedName("ongoing_task")
	val ongoingTask: Int? = null,

	@field:SerializedName("active_task")
	val activeTask: Int? = null,

	@field:SerializedName("progress_bar")
	val progressBar: Int? = null,

	@field:SerializedName("created_by")
	val createdBy: Int? = null,

	@field:SerializedName("complete_task")
	val completeTask: Int? = null,

	@field:SerializedName("assign_clients")
	val assignClients: List<AssignClientsItem?>? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("stages")
	val stages: List<StagesItem?>? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("assign_users")
	val assignUsers: List<AssignUsersItem?>? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("start_date")
	val startDate: String? = null
)

data class ClientsItem(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("pivot")
	val pivot: Pivot? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("avatar")
	val avatar: String? = null,

	@field:SerializedName("email")
	val email: String? = null
)

data class Status(

	@field:SerializedName("Finished")
	val finished: String? = null,

	@field:SerializedName("OnHold")
	val onHold: String? = null,

	@field:SerializedName("Ongoing")
	val ongoing: String? = null
)

data class JsonMember0(

	@field:SerializedName("end_date")
	val endDate: String? = null,

	@field:SerializedName("owner_email")
	val ownerEmail: String? = null,

	@field:SerializedName("owner_name")
	val ownerName: String? = null,

	@field:SerializedName("total_tasks")
	val totalTasks: Int? = null,

	@field:SerializedName("ongoing_task")
	val ongoingTask: Int? = null,

	@field:SerializedName("active_task")
	val activeTask: Int? = null,

	@field:SerializedName("progress_bar")
	val progressBar: Int? = null,

	@field:SerializedName("created_by")
	val createdBy: Int? = null,

	@field:SerializedName("complete_task")
	val completeTask: Int? = null,

	@field:SerializedName("assign_clients")
	val assignClients: List<AssignClientsItem?>? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("stages")
	val stages: List<StagesItem?>? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("assign_users")
	val assignUsers: List<AssignUsersItem?>? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("start_date")
	val startDate: String? = null
)

data class AssignClientsItem(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("pivot")
	val pivot: Pivot? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("avatar")
	val avatar: String? = null
)

data class ProjectsData(

	@field:SerializedName("projects")
	val projects: Projects? = null,

	@field:SerializedName("clients")
	val clients: ArrayList<ClientsItem>? = null,

	@field:SerializedName("users")
	val users: ArrayList<UsersItem>? = null,

	@field:SerializedName("status")
	val status: Status? = null,

	@field:SerializedName("0")
	val jsonMember0: JsonMember0? = null,

	@field:SerializedName("1")
	val jsonMember1: JsonMember1? = null,

	@field:SerializedName("2")
	val jsonMember2: JsonMember2? = null,

	@field:SerializedName("5")
	val jsonMember5: JsonMember5? = null
)



data class JsonMember1(

	@field:SerializedName("end_date")
	val endDate: String? = null,

	@field:SerializedName("owner_email")
	val ownerEmail: String? = null,

	@field:SerializedName("owner_name")
	val ownerName: String? = null,

	@field:SerializedName("total_tasks")
	val totalTasks: Int? = null,

	@field:SerializedName("ongoing_task")
	val ongoingTask: Int? = null,

	@field:SerializedName("active_task")
	val activeTask: Int? = null,

	@field:SerializedName("progress_bar")
	val progressBar: Int? = null,

	@field:SerializedName("created_by")
	val createdBy: Int? = null,

	@field:SerializedName("complete_task")
	val completeTask: Int? = null,

	@field:SerializedName("assign_clients")
	val assignClients: List<AssignClientsItem?>? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("stages")
	val stages: List<StagesItem?>? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("assign_users")
	val assignUsers: List<AssignUsersItem?>? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("start_date")
	val startDate: String? = null
)

data class AssignUsersItem(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("pivot")
	val pivot: Pivot? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("avatar")
	val avatar: String? = null
)

data class JsonMember5(

	@field:SerializedName("end_date")
	val endDate: String? = null,

	@field:SerializedName("owner_email")
	val ownerEmail: String? = null,

	@field:SerializedName("owner_name")
	val ownerName: String? = null,

	@field:SerializedName("total_tasks")
	val totalTasks: Int? = null,

	@field:SerializedName("ongoing_task")
	val ongoingTask: Int? = null,

	@field:SerializedName("active_task")
	val activeTask: Int? = null,

	@field:SerializedName("progress_bar")
	val progressBar: Int? = null,

	@field:SerializedName("created_by")
	val createdBy: Int? = null,

	@field:SerializedName("complete_task")
	val completeTask: Int? = null,

	@field:SerializedName("assign_clients")
	val assignClients: List<AssignClientsItem?>? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("stages")
	val stages: List<StagesItem?>? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("assign_users")
	val assignUsers: List<AssignUsersItem?>? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("start_date")
	val startDate: String? = null
)

data class Projects(

	@field:SerializedName("per_page")
	val perPage: Int? = null,

	@field:SerializedName("data")
	val data: ProjectsData? = null,

	@field:SerializedName("last_page")
	val lastPage: Int? = null,

	@field:SerializedName("next_page_url")
	val nextPageUrl: String? = null,

	@field:SerializedName("prev_page_url")
	val prevPageUrl: Any? = null,

	@field:SerializedName("first_page_url")
	val firstPageUrl: String? = null,

	@field:SerializedName("path")
	val path: String? = null,

	@field:SerializedName("total")
	val total: Int? = null,

	@field:SerializedName("last_page_url")
	val lastPageUrl: String? = null,

	@field:SerializedName("from")
	val from: Int? = null,

	@field:SerializedName("links")
	val links: ArrayList<LinksItem>? = null,

	@field:SerializedName("to")
	val to: Int? = null,

	@field:SerializedName("current_page")
	val currentPage: Int? = null
)

data class UsersItem(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("pivot")
	val pivot: Pivot? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("avatar")
	val avatar: String? = null,

	@field:SerializedName("email")
	val email: String? = null
)
