package com.workdo.taskly.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.workdo.taskly.R
import com.workdo.taskly.databinding.ActSlackSettingsBinding

class ActSlackSettings : AppCompatActivity() {

    private lateinit var binding: ActSlackSettingsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActSlackSettingsBinding.inflate(layoutInflater)
        setContentView(binding.root)


        binding.ivBack.setOnClickListener {
            finish()
        }
    }
}