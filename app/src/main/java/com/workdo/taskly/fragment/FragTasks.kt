package com.workdo.taskly.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.workdo.taskly.adapter.DashBoardTaskListAdapter
import com.workdo.taskly.adapter.SummeryTaskAdapter
import com.workdo.taskly.adapter.TaskCategoryAdapter
import com.workdo.taskly.databinding.FragTasksBinding
import com.workdo.taskly.model.DashboardTasksDataItem
import com.workdo.taskly.util.Constants


class FragTasks : Fragment() {
    private var taskList=ArrayList<DashboardTasksDataItem>()
    private lateinit var binding: FragTasksBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragTasksBinding.inflate(layoutInflater)
        setAdapter()
        return binding.root
    }


    private fun setAdapter() {
        binding.rvTaskCategory.apply {
            layoutManager = LinearLayoutManager(requireActivity(), RecyclerView.HORIZONTAL, false)

            adapter = TaskCategoryAdapter()
            isNestedScrollingEnabled = true
            val snapHelper = PagerSnapHelper()
            snapHelper.attachToRecyclerView(this)
        }
        binding.rvTaskList.apply {
            layoutManager = LinearLayoutManager(requireActivity(), RecyclerView.VERTICAL, false)
            adapter = DashBoardTaskListAdapter(taskList) { type: String, position: Int ->
                run {
                    if (type == Constants.ItemClick) {

                    }
                }
                isNestedScrollingEnabled = true
            }

        }

        binding.rvSummeryTask.apply {
            layoutManager = LinearLayoutManager(requireActivity(), RecyclerView.VERTICAL, false)
            adapter = SummeryTaskAdapter()
            isNestedScrollingEnabled=true
            itemAnimator= DefaultItemAnimator()
        }


    }
}