package com.workdo.taskly.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.workdo.taskly.databinding.CellInvoicesBinding
import com.workdo.taskly.model.MenuItemData
import com.workdo.taskly.util.Constants

class InvoicesAdapter(var listener:(String,Int)->Unit) : RecyclerView.Adapter<InvoicesAdapter.InvoicesViewHolder>() {

    inner class InvoicesViewHolder(var itemBinding: CellInvoicesBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bindItem(listener:(String,Int)->Unit,position: Int)
        {
            itemView.setOnClickListener {
                listener(Constants.ItemClick,position)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InvoicesViewHolder {
        val view = CellInvoicesBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return InvoicesViewHolder(view)
    }

    override fun onBindViewHolder(holder: InvoicesViewHolder, position: Int) {
        holder.bindItem(listener,position)
    }

    override fun getItemCount(): Int {
        return 3
    }
}