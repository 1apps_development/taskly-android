package com.workdo.taskly.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.workdo.taskly.databinding.ActPlansBinding

class ActPlans : AppCompatActivity() {
    private lateinit var binding:ActPlansBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActPlansBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }


}