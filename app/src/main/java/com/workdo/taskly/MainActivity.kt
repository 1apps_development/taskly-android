package com.workdo.taskly

import android.app.Dialog
import android.content.res.Resources
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.GridLayoutManager
import com.github.techisfun.android.topsheet.TopSheetBehavior
import com.github.techisfun.android.topsheet.TopSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.workdo.taskly.adapter.InvoicesAdapter
import com.workdo.taskly.adapter.MenuAdapter
import com.workdo.taskly.databinding.ActivityMainBinding
import com.workdo.taskly.databinding.DialogMenuBinding
import com.workdo.taskly.databinding.InvoiceDetailDialogBinding
import com.workdo.taskly.databinding.NewClientDialogBinding
import com.workdo.taskly.fragment.*
import com.workdo.taskly.model.MenuItemData
import com.workdo.taskly.util.Common

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private var itemList = ArrayList<MenuItemData>()
    private lateinit var menuAdapter: MenuAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)


        Common.replaceFragment(supportFragmentManager, FragDashBoard(), R.id.fragContainer)
        initClickListeners()
        setContentView(binding.root)



        menuData()

        binding.ivDrawer.setOnClickListener {
            openTopSheetDialog()
        }

        binding.tvAppBarTitle.text = resources.getString(R.string.dashboard)
        bottomSheetItemNavigation()
        binding.bottomNavigation.selectedItemId = R.id.ivDashBoard

    }


    private fun menuData()
    {
        itemList.add(
            MenuItemData(
                resources.getString(R.string.dashboard),
                ResourcesCompat.getDrawable(resources, R.drawable.ic_dashboard, null)!!, true
            )
        )
        itemList.add(
            MenuItemData(
                resources.getString(R.string.tasks),
                ResourcesCompat.getDrawable(resources, R.drawable.ic_task_stages, null)!!
            )
        )
        itemList.add(
            MenuItemData(
                resources.getString(R.string.zoom_meeting_),
                ResourcesCompat.getDrawable(resources, R.drawable.ic_zoom, null)!!
            )
        )
        itemList.add(
            MenuItemData(
                resources.getString(R.string.invoices),
                ResourcesCompat.getDrawable(resources, R.drawable.ic_invoices, null)!!
            )
        )
        itemList.add(
            MenuItemData(
                resources.getString(R.string.users),
                ResourcesCompat.getDrawable(resources, R.drawable.ic_user, null)!!
            )
        )
        itemList.add(
            MenuItemData(
                resources.getString(R.string.notes),
                ResourcesCompat.getDrawable(resources, R.drawable.ic_notes, null)!!
            )
        )
        itemList.add(
            MenuItemData(
                resources.getString(R.string.projects),
                ResourcesCompat.getDrawable(resources, R.drawable.ic_project, null)!!
            )
        )
        itemList.add(
            MenuItemData(
                resources.getString(R.string.settings),
                ResourcesCompat.getDrawable(resources, R.drawable.ic_settings, null)!!
            )
        )
        itemList.add(
            MenuItemData(
                resources.getString(R.string.clients),
                ResourcesCompat.getDrawable(resources, R.drawable.ic_clients, null)!!
            )
        )


        itemList.add(
            MenuItemData(
                resources.getString(R.string.timesheet),
                ResourcesCompat.getDrawable(resources, R.drawable.ic_timesheet, null)!!
            )
        )


        itemList.add(
            MenuItemData(
                resources.getString(R.string.subscription),
                ResourcesCompat.getDrawable(resources, R.drawable.ic_save, null)!!
            )
        )


        itemList.add(
            MenuItemData(
                resources.getString(R.string.plans),
                ResourcesCompat.getDrawable(resources, R.drawable.ic_plan, null)!!
            )
        )

        itemList.add(
            MenuItemData(
                resources.getString(R.string.calender),
                ResourcesCompat.getDrawable(resources, R.drawable.ic_calender, null)!!
            )
        )

        itemList.add(
            MenuItemData(
                resources.getString(R.string.tracker),
                ResourcesCompat.getDrawable(resources, R.drawable.ic_clock, null)!!
            )
        )

    }


    private fun menuSelection(pos: Int) {
        for (j in 0 until itemList.size) {
            itemList[j].isSelect = false

        }
        itemList[pos].isSelect = true
    }

    private fun initClickListeners() {
        binding.notificationLayout.setOnClickListener {
            val fragment = supportFragmentManager.findFragmentById(R.id.fragContainer)
            binding.tvAppBarTitle.text = resources.getString(R.string.notifications)
            if (fragment !is FragNotifications) {
                Common.replaceFragment(
                    supportFragmentManager,
                    FragNotifications(),
                    R.id.fragContainer
                )
            }
        }
    }


    private fun openTopSheetDialog() {
        val dialog = TopSheetDialog(this)
        val bottomSheetBinding = DialogMenuBinding.inflate(layoutInflater)

        menuAdapter = MenuAdapter(this@MainActivity, itemList) { s: String, i: Int ->
            if (s == "itemClick") {
                val fragment = supportFragmentManager.findFragmentById(R.id.fragContainer)
                dialog.dismiss()

                if (i == 0) {
                    binding.tvAppBarTitle.text = resources.getString(R.string.dashboard)
                    if (fragment !is FragDashBoard) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragDashBoard(),
                            R.id.fragContainer
                        )
                    }
                } else if (i == 1) {
                    binding.tvAppBarTitle.text = resources.getString(R.string.tasks)
                    if (fragment !is FragTasks) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragTasks(),
                            R.id.fragContainer
                        )
                    }
                } else if (i == 2) {
                    binding.tvAppBarTitle.text = resources.getString(R.string.zoom_meetings)
                    if (fragment !is FragZoomMeetings) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragZoomMeetings(),
                            R.id.fragContainer
                        )
                    }
                } else if (i == 3) {
                    binding.tvAppBarTitle.text = resources.getString(R.string.invoices)
                    if (fragment !is FragInvoices) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragInvoices(),
                            R.id.fragContainer
                        )
                    }
                } else if (i == 4) {
                    binding.tvAppBarTitle.text = resources.getString(R.string.users)
                    if (fragment !is FragUsers) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragUsers(),
                            R.id.fragContainer
                        )
                    }
                } else if (i == 5) {
                    binding.tvAppBarTitle.text = resources.getString(R.string.notes)
                    if (fragment !is FragNotes) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragNotes(),
                            R.id.fragContainer
                        )
                    }
                } else if (i == 6) {
                    binding.tvAppBarTitle.text = resources.getString(R.string.projects)
                    if (fragment !is FragProjects) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragProjects(),
                            R.id.fragContainer
                        )
                    }
                } else if (i == 7) {
                    binding.tvAppBarTitle.text = resources.getString(R.string.settings)
                    if (fragment !is FragSettings) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragSettings(),
                            R.id.fragContainer
                        )
                    }
                } else if (i == 8) {
                    binding.tvAppBarTitle.text = resources.getString(R.string.clients)
                    if (fragment !is FragClients) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragClients(),
                            R.id.fragContainer
                        )
                    }
                } else if(i==9) {

                    binding.tvAppBarTitle.text = resources.getString(R.string.timesheet)
                    if (fragment !is FragTimeSheet) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragTimeSheet(),
                            R.id.fragContainer
                        )
                    }

                } else if(i==10) {

                    binding.tvAppBarTitle.text = resources.getString(R.string.subscription)
                    if (fragment !is FragSubscription) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragSubscription(),
                            R.id.fragContainer
                        )
                    }

                }else if(i==11) {

                    binding.tvAppBarTitle.text = resources.getString(R.string.plans)
                    if (fragment !is FragPlans) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragPlans(),
                            R.id.fragContainer
                        )
                    }

                }
                else if(i==12) {

                    binding.tvAppBarTitle.text = resources.getString(R.string.calender)
                    if (fragment !is FragCalender) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragCalender(),
                            R.id.fragContainer
                        )
                    }

                }

                else if(i==13) {

                    binding.tvAppBarTitle.text = resources.getString(R.string.tracker)
                    if (fragment !is FragTracker) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragTracker(),
                            R.id.fragContainer
                        )
                    }

                }

                for (j in 0 until itemList.size) {
                    itemList[j].isSelect = false

                }
                itemList[i].isSelect = true
                menuAdapter.notifyDataSetChanged()
            }

        }
        bottomSheetBinding.ivClose.setOnClickListener {
            dialog.dismiss()
        }
        bottomSheetBinding.rvMenu.apply {
            layoutManager = GridLayoutManager(this@MainActivity, 4)
            adapter = menuAdapter
            isNestedScrollingEnabled = true
        }
        dialog.setContentView(bottomSheetBinding.root)
        dialog.show()
    }

    private fun bottomSheetItemNavigation() {
        binding.bottomNavigation.setOnItemSelectedListener { item ->
            val fragment = supportFragmentManager.findFragmentById(R.id.fragContainer)
            when (item.itemId) {
                R.id.ivTask -> {
                    binding.tvAppBarTitle.text = resources.getString(R.string.tasks)
                    menuSelection(1)
                    if (fragment !is FragTasks) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragTasks(),
                            R.id.fragContainer
                        )
                    }

                    true


                }
                R.id.ivInvoice -> {
                    menuSelection(3)

                    binding.tvAppBarTitle.text = resources.getString(R.string.invoices)

                    Common.replaceFragment(
                        supportFragmentManager,
                        FragInvoices(),
                        R.id.fragContainer
                    )

                    true
                }
                R.id.ivDashBoard -> {
                    menuSelection(0)

                    binding.tvAppBarTitle.text = resources.getString(R.string.dashboard)

                    Common.replaceFragment(
                        supportFragmentManager,
                        FragDashBoard(),
                        R.id.fragContainer
                    )

                    true
                }
                R.id.ivUser -> {
                    menuSelection(4)

                    binding.tvAppBarTitle.text = resources.getString(R.string.users)

                    Common.replaceFragment(supportFragmentManager, FragUsers(), R.id.fragContainer)

                    true
                }

                R.id.ivSetting -> {
                    menuSelection(7)

                    binding.tvAppBarTitle.text = resources.getString(R.string.users)

                    Common.replaceFragment(
                        supportFragmentManager,
                        FragSettings(),
                        R.id.fragContainer
                    )
                    binding.tvAppBarTitle.text = resources.getString(R.string.settings)

                    true
                }
                else -> {
                    false
                }
            }
        }
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount == 1) {
            finishAffinity()
        }else
        {
            supportFragmentManager.popBackStack()
            super.onBackPressed()
        }
    }

        fun setAppBarTitle(title:String)
        {
            binding.tvAppBarTitle.text=title
        }

    /*Todo Menu Dialog*/
    private fun menuDialog() {
        val dialog = Dialog(this@MainActivity)
        val bottomSheetBinding = DialogMenuBinding.inflate(layoutInflater)
//        TopSheetBehavior.from(bottomSheetBinding.root).state=TopSheetBehavior.STATE_EXPANDED

        val menuAdapter = MenuAdapter(this@MainActivity, itemList) { s: String, i: Int ->
            if (s == "itemClick") {
                if (i == 9) {

                }
            }
        }
        bottomSheetBinding.rvMenu.apply {
            layoutManager = GridLayoutManager(this@MainActivity, 4)
            adapter = menuAdapter
        }

        bottomSheetBinding.ivClose.setOnClickListener {
            dialog.dismiss()
        }
        dialog.setContentView(bottomSheetBinding.root)

        /*   val width = (resources.displayMetrics.widthPixels * 0.90).toInt()
           val height = (resources.displayMetrics.heightPixels * 0.80).toInt()*/
        val height = Resources.getSystem().displayMetrics.heightPixels / 2
        dialog.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, height)

        dialog.show()
    }

}