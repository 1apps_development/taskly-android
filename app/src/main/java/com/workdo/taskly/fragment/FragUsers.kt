package com.workdo.taskly.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.workdo.taskly.R
import com.workdo.taskly.adapter.UsersAdapter
import com.workdo.taskly.databinding.FragUsersBinding
import com.workdo.taskly.databinding.NewUserDialogBinding
import com.workdo.taskly.model.UserDataItem
import com.workdo.taskly.networking.ApiClient
import com.workdo.taskly.remote.NetworkResponse
import com.workdo.taskly.util.Utils
import kotlinx.coroutines.launch


class FragUsers : Fragment() {
    private var _binding: FragUsersBinding? = null
    private val binding get() = _binding!!
    private var userList=ArrayList<UserDataItem>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragUsersBinding.inflate(layoutInflater)

        initClickListeners()
        return binding.root
    }

    private fun setUpAdapter() {
        binding.rvUsers.apply {
            layoutManager = LinearLayoutManager(requireActivity())
            adapter = UsersAdapter(userList)
            isNestedScrollingEnabled = true
        }


    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getUserListApi()
    }

    private fun initClickListeners() {

        binding.btnAddNewUser.setOnClickListener {
            newUserDialog()

        }

    }


    private fun getUserListApi() {
        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity()).manageUser()) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()

                    when (response.body.status) {
                        1 -> {
                            response.body.data?.data?.let { userList.addAll(it) }
                            setUpAdapter()
                        }

                        0 -> {
                            Utils.errorAlert(requireActivity(), response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), response.body.message.toString())

                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(),
                        resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), "Something went wrong")
                }
            }
        }


    }


    private fun newUserDialog() {
        val bottomSheetDialog = BottomSheetDialog(requireActivity(), R.style.BottomSheetDialogTheme)


        val bottomSheetBinding = NewUserDialogBinding.inflate(layoutInflater)
        bottomSheetDialog.setContentView(bottomSheetBinding.root)
        bottomSheetBinding.btnClose.setOnClickListener {
            bottomSheetDialog.dismiss()
        }
        bottomSheetBinding.tvClose.setOnClickListener {
            bottomSheetDialog.dismiss()
        }

        bottomSheetDialog.show()

    }


}