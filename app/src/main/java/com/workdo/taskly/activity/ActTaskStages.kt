package com.workdo.taskly.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.appcompat.widget.AppCompatImageButton
import com.workdo.taskly.R
import com.workdo.taskly.databinding.ActTaskStagesBinding

class ActTaskStages : AppCompatActivity() {
    private lateinit var binding:ActTaskStagesBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActTaskStagesBinding.inflate(layoutInflater)
        initClickListener()
        initView()
        setContentView(binding.root)
    }

    private fun initView()
    {
        for(i  in 0 until 3)
        {
            addNewView()
            addBugView()
        }
    }

    private fun initClickListener() {
        binding.ivBack.setOnClickListener {
            finish()
        }

        binding.btnBugCreate.setOnClickListener {
            addBugView()
        }
        binding.btnCreateStage.setOnClickListener {
            addNewView()
        }

    }
    private fun addNewView() {
        val inflater = LayoutInflater.from(this@ActTaskStages).inflate(R.layout.cell_stages, null)

        val ivDelete: AppCompatImageButton = inflater.findViewById(R.id.ivDelete)



        ivDelete.setOnClickListener {
            onDelete(inflater)

        }

        val layoutParams: LinearLayout.LayoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        layoutParams.topMargin = 12
        inflater.layoutParams = layoutParams
        binding.viewList.addView(inflater, binding.viewList.childCount)

    }


    private fun addBugView() {
        val inflater = LayoutInflater.from(this@ActTaskStages).inflate(R.layout.cell_stages, null)

        val ivDelete: AppCompatImageButton = inflater.findViewById(R.id.ivDelete)



        ivDelete.setOnClickListener {
            onDeleteFromBug(inflater)

        }

        val layoutParams: LinearLayout.LayoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        layoutParams.topMargin = 12
        inflater.layoutParams = layoutParams
        binding.bugViewList.addView(inflater, binding.bugViewList.childCount)

    }

    private fun onDelete(v: View) {

        binding.viewList.removeView(v)
    }

    private fun onDeleteFromBug(v: View) {

        binding.bugViewList.removeView(v)
    }
}