package com.workdo.taskly.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.workdo.taskly.databinding.CellTaskSummeryBinding

class SummeryTaskAdapter : RecyclerView.Adapter<SummeryTaskAdapter.SummeryTask>() {


    inner class SummeryTask(var itemBinding: CellTaskSummeryBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SummeryTask {
        val view =
            CellTaskSummeryBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SummeryTask(view)
    }

    override fun onBindViewHolder(holder: SummeryTask, position: Int) {
    }

    override fun getItemCount(): Int {
        return 5
    }
}