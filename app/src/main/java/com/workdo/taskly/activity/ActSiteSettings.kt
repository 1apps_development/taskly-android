package com.workdo.taskly.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.workdo.taskly.R
import com.workdo.taskly.databinding.ActSiteSettingsBinding

class ActSiteSettings : AppCompatActivity() {
    private lateinit var binding:ActSiteSettingsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActSiteSettingsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.ivBack.setOnClickListener {
            finish()
        }
    }
}