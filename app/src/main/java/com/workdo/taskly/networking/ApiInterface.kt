package com.workdo.taskly.networking

import com.workdo.taskly.model.*
import com.workdo.taskly.remote.NetworkResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface ApiInterface {

    @POST("applogin")
    suspend fun getLogin(@Body map: HashMap<String, String>): NetworkResponse<LoginResponse,SingleResponse>

    @POST("register")
    suspend fun register(@Body map: HashMap<String, String>): NetworkResponse<RegisterResponse,SingleResponse>

    @POST("logout")
    suspend fun logOut():  NetworkResponse<RegisterResponse,SingleResponse>

    @POST("change-password")
    suspend fun changePassword(@Body map: HashMap<String, String>): NetworkResponse<SingleResponse,SingleResponse>


    @POST("profile-setting")
    suspend fun profileUpdate(@Part("name") name: RequestBody,
                              @Part("email") email:RequestBody,
                              @Part avatar: MultipartBody.Part?):  NetworkResponse<SingleResponse,SingleResponse>


    @POST("manage-user")
    suspend fun manageUser():NetworkResponse<UserListResponse,SingleResponse>

    @POST("dashboard")
    suspend fun dashBoard(@Body request:HashMap<String,String>):NetworkResponse<DashBoardResponse,SingleResponse>


    @POST("manage-projects")
    suspend fun projectList():NetworkResponse<ProjectListResponse,SingleResponse>


    @POST("manage-client")
    suspend fun clientList():NetworkResponse<ClientListResponse,SingleResponse>

    @POST("create-client")
    suspend fun createClient(@Body map: HashMap<String, String>):NetworkResponse<SingleResponse,SingleResponse>

    @POST("edit-client")
    suspend fun editClient(@Body map: HashMap<String, String>):NetworkResponse<SingleResponse,SingleResponse>


    @POST("update-client")
    suspend fun updateClient(@Body map: HashMap<String, String>):NetworkResponse<SingleResponse,SingleResponse>

    @POST("delete-client")
    suspend fun deleteClient(@Body map: HashMap<String, String>):NetworkResponse<SingleResponse,SingleResponse>
}