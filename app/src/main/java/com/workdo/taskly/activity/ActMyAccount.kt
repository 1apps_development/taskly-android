package com.workdo.taskly.activity

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import com.workdo.taskly.R
import com.workdo.taskly.databinding.ActMyAccountBinding
import com.workdo.taskly.networking.ApiClient
import com.workdo.taskly.remote.NetworkResponse
import com.workdo.taskly.util.Utils
import kotlinx.coroutines.launch
import java.io.File

class ActMyAccount : AppCompatActivity() {
    private var _binding: ActMyAccountBinding? = null
    private val binding get() = _binding!!

    private var imageFile: File? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActMyAccountBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initClickListeners()
    }


    private fun initClickListeners() {
        binding.btnUpdatePassword.setOnClickListener {
            if (binding.edOldPassword.text?.isNotEmpty() == true) {
                Utils.errorAlert(this@ActMyAccount, getString(R.string.old_password_error))
            } else if (binding.edPassword.text?.isNotEmpty() == true) {
                Utils.errorAlert(this@ActMyAccount, getString(R.string.password_error))
            } else if (binding.edConfirmPassword.text?.isNotEmpty() == true) {
                Utils.errorAlert(this@ActMyAccount, getString(R.string.confirm_password_error))
            } else if (binding.edConfirmPassword.text.toString() == binding.edPassword.text.toString()) {
                callChangePassword()
            }
        }

        binding.tvChangeProfile.setOnClickListener {
            ImagePicker.with(this)
                .cropSquare()
                .compress(1024)
                .saveDir(
                    File(
                        getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                        resources.getString(R.string.app_name)
                    )
                )
                .maxResultSize(1080, 1080)
                .createIntent { intent ->
                    startForProfileImageResult.launch(intent)
                }
        }


        binding.btnUpdateProfile.setOnClickListener {

            if (binding.edFullname.text.toString().isEmpty()) {
                Utils.errorAlert(this@ActMyAccount, "please enter full name")
            } else if (binding.edEmail.text.toString().isEmpty()) {
                Utils.errorAlert(this@ActMyAccount, "please enter email")
            } else if (!Utils.isValidEmail(binding.edEmail.text.toString())) {
                Utils.errorAlert(this@ActMyAccount, "please enter valid email")
            } else {
                callProfileUpdate()
            }

        }
    }

    private fun callChangePassword() {
        val request = HashMap<String, String>()
        request["old_password"] = binding.edOldPassword.text.toString()
        request["password"] = binding.edPassword.text.toString()
        request["password_confirmation"] = binding.edPassword.text.toString()

        Utils.showLoadingProgress(this@ActMyAccount)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActMyAccount).changePassword(request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()

                    when (response.body.status) {
                        1 -> {
                            Utils.successAlert(this@ActMyAccount, "Password Successfully changed")


                        }

                        0 -> {
                            Utils.errorAlert(this@ActMyAccount, response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActMyAccount, response.body.message.toString())

                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActMyAccount,
                        resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActMyAccount, "Something went wrong")
                }
            }
        }
    }


    // Todo OnActivityResult Profile  Picture Update
    private val startForProfileImageResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            val resultCode = result.resultCode
            val data = result.data
            when (resultCode) {
                Activity.RESULT_OK -> {
                    val fileUri = data?.data!!
                    Log.e("FilePath", fileUri.path.toString())
                    fileUri.path.let { imageFile = File(it) }
                    Log.e("imageFileLength", imageFile!!.length().toString())
                    Glide.with(this@ActMyAccount).load(fileUri.path)
                        .into(binding.ivProfile)
                }
                ImagePicker.RESULT_ERROR -> {
                    Toast.makeText(
                        this@ActMyAccount,
                        ImagePicker.getError(data),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }


    private fun callProfileUpdate() {


        val name = Utils.setRequestBody(binding.edFullname.text.toString())
        val email = Utils.setRequestBody(binding.edEmail.text.toString())
        val image = imageFile?.let { Utils.setImageUpload("avatar", it) }
        Utils.showLoadingProgress(this@ActMyAccount)
        lifecycleScope.launch {
            when (val response =
                ApiClient.getClient(this@ActMyAccount).profileUpdate(name, email, image)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()

                    when (response.body.status) {
                        1 -> {
                            Utils.successAlert(this@ActMyAccount, "Password Successfully changed")
                        }

                        0 -> {
                            Utils.errorAlert(this@ActMyAccount, response.body.message.toString())
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActMyAccount, response.body.message.toString())

                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActMyAccount,
                        resources.getString(R.string.internet_connection_error))
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActMyAccount, "Something went wrong")
                }
            }
        }


    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}