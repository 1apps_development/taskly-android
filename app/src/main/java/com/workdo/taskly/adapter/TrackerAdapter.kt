package com.workdo.taskly.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.workdo.taskly.databinding.CellTrackerBinding

class TrackerAdapter : RecyclerView.Adapter<TrackerAdapter.TrackerViewHolder>() {


    inner class TrackerViewHolder(var tackerBinding: CellTrackerBinding) :
        RecyclerView.ViewHolder(tackerBinding.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TrackerViewHolder {
        val view = CellTrackerBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return TrackerViewHolder(view)
    }

    override fun onBindViewHolder(holder: TrackerViewHolder, position: Int) {
    }

    override fun getItemCount(): Int {

        return 4
    }

}